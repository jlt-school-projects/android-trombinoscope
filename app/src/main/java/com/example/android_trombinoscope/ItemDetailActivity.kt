package com.example.android_trombinoscope

import android.Manifest
import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.telephony.SmsManager
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.core.app.ActivityCompat
import com.example.android_trombinoscope.model.Trombi
import kotlinx.android.synthetic.main.activity_item_detail.*
import kotlinx.android.synthetic.main.send_message_dialog.*
import kotlinx.android.synthetic.main.send_message_dialog.view.*

/**
 * An activity representing a single Item detail screen. This
 * activity is only used on narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a [ItemListActivity].
 */
class ItemDetailActivity : AppCompatActivity() {

    val SMS_PERMISSION_REQUEST= 40;
    var item: Trombi? = null;
    var number: String? = null;
    var message: String? = null;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_detail)
        setSupportActionBar(detail_toolbar)

        fab.setOnClickListener { view ->
            onClickEnvoiSMS(view);
        }

        // Show the Up button in the action bar.
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //
        item = intent.getSerializableExtra(ItemDetailFragment.ARG_ITEM_ID) as Trombi;
        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            val fragment = ItemDetailFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(
                        ItemDetailFragment.ARG_ITEM_ID,
                        intent.getSerializableExtra(ItemDetailFragment.ARG_ITEM_ID)
                    )
                }
            }

            supportFragmentManager.beginTransaction()
                .add(R.id.item_detail_container, fragment)
                .commit()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) =
        when (item.itemId) {
            android.R.id.home -> {
                // This ID represents the Home or Up button. In the case of this
                // activity, the Up button is shown. For
                // more details, see the Navigation pattern on Android Design:
                //
                // http://developer.android.com/design/patterns/navigation.html#up-vs-back

                navigateUpTo(Intent(this, ItemListActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }


    fun onClickEnvoiSMS(view: View){
            val dialog: Dialog = AlertDialog.Builder(this)
                .setTitle(R.string.send_dialog_title)
                .setView(layoutInflater.inflate(R.layout.send_message_dialog, null))
                .setPositiveButton(R.string.send,
                    DialogInterface.OnClickListener { dialog, which ->
                        this.number = (dialog as Dialog).findViewById<TextView>(R.id.phoneNumberField).text.toString();
                        this.message = (dialog as Dialog).findViewById<TextView>(R.id.messageField).text.toString();
                        ActivityCompat.requestPermissions(
                            this,
                            arrayOf(Manifest.permission.SEND_SMS, Manifest.permission.READ_PHONE_STATE),
                            SMS_PERMISSION_REQUEST
                        )
                    })
                .setNegativeButton(R.string.delete, null)
                .create();
            dialog.show();
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if(requestCode == SMS_PERMISSION_REQUEST){
            if(grantResults.size == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED){
                sendSms(this.number, this.message);
            }
        }
    }

    private fun sendSms(to: String?, message: String?) {
        val manager = SmsManager.getDefault();
        manager.sendTextMessage(to, null, "${message} ", null, null)
    }


}
