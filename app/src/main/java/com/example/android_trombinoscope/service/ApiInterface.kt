package com.example.android_trombinoscope.service

import com.example.android_trombinoscope.model.Trombi
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface ApiInterface {
    @GET("trombi.php")
    fun getTrombi() : Call<List<Trombi>>


    companion object {
        val instance: ApiInterface by lazy {
            val gsonBuilder = GsonBuilder();
            val gson = gsonBuilder.create();
            val retrofit = Retrofit.Builder()
                .baseUrl("http://student.ddadev.fr/dev_mobile/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
            retrofit.create(ApiInterface::class.java)
        }
    }
}